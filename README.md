# Copier PDM

Copier template for Python projects managed by PDM.

This copier template is mainly for my own usage,
but feel free to try it out, or fork it!

Also checkout [copier-poetry](https://github.com/pawamoy/copier-poetry),
which is the same template
but with [Poetry](https://github.com/python-poetry/poetry) instead of PDM.

See [how to migrate to copier-pdm](https://pawamoy.github.io/copier-pdm/migrate).

## Features

- [PDM](https://github.com/pdm-project/pdm) setup, with pre-defined `pyproject.toml`
- Documentation built with [MkDocs](https://github.com/mkdocs/mkdocs)
  ([Material theme](https://github.com/squidfunk/mkdocs-material)
  and "autodoc" [mkdocstrings plugin](https://github.com/pawamoy/mkdocstrings))
- Pre-configured tools for code formatting, quality analysis and testing:
    - [black](https://github.com/psf/black),
    - [flake8](https://gitlab.com/pycqa/flake8) and plugins,
    - [isort](https://github.com/timothycrosley/isort),
    - [mypy](https://github.com/python/mypy),
    - [safety](https://github.com/pyupio/safety)
- Tests run with [pytest](https://github.com/pytest-dev/pytest) and plugins, with [coverage](https://github.com/nedbat/coveragepy) support
- Cross-platform tasks with [duty](https://github.com/pawamoy/duty)
- Support for GitHub workflow and Gitlab CI
- Python 3.6 or above
- Auto-generated `CHANGELOG.md` from git commits (using Angular message style)
- Auto-generated `CREDITS.md` from Python dependencies
- All licenses from [choosealicense.com](https://choosealicense.com/appendix/)
- Makefile for convenience

## Quick setup and usage

Make sure all the
[requirements](https://pawamoy.github.io/copier-pdm/requirements)
are met, then:

```bash
copier "https://gitlab.com/mikeramsey/copier-pdm-fbs.git" /path/to/your/new/project
```

Or even shorter:

```bash
copier "gl:mikeramsey/copier-pdm-fbs" /path/to/your/new/project
```

See the [documentation](https://pawamoy.github.io/copier-pdm)
for more details.

## Modifying the template skeleton usage
Once pdm venv is installed you can run the below to see the skeleton mainwindow.
```bash
fbs run
```

You can then add all your custom widgets and signals and slots easily to the `src/main/python/{{your_app}}/main.py` file.

To edit the GUI just open `src/main/python/{{your_app}}/ui/mainwindow.ui` file in Qt Designer and save and then rerun fbs run to see changes instantly.

## Known Issues
If using python 3.7 or higher with fbs it may complain due to the super low pinned pyinstaller version in fbs 0.9.0 which was the last one updated before pro. You can easily fix this by running the below which will ignore the dependency pinning to lower pyinstaller==3.4.
```bash
pdm run pip install pyinstaller==4.2
```

It is a workaround, but is the best way to address that issue as poetry does not yet offer a clean way to ignore/override sub-dependencies.
Reference: https://github.com/python-poetry/poetry/issues/697

If you run a pdm update/install, you will want to just rerun the above command to upgrade pyinstaller forcibly to later version which supports python 3.7+ before trying to fbs freeze/installer/release.

## Helpful Resources
[fbs-documentation](https://build-system.fman.io/) : FBS documentation for the fbs specific details.

[LearnPyQt](https://www.learnpyqt.com/) : Has some of the best video tutorials and up to date resources for PyQT/Pyside.
